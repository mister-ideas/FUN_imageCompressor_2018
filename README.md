# Subject
From a file listing all the pixels of the image with their position and color, regroup them into a given number of clusters, according to their colors. You HAVE to use the k-means algorithm for clustering. Remember how k-means work? At every step, compute a new bunch of k centroids andadd data to its nearest centroid, as in a Voronoi space, until the algorithm converges. You must use the provided input and output syntaxes.

# Building
$ make

# Usage
$ ./imageCompressor [file_path]

# Mark
**17,2/20**
