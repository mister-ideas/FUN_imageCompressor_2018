##
## EPITECH PROJECT, 2019
## FUN_imageCompressor_2018
## File description:
## Makefile
##

EXEC_PATH=		.stack-work/install/x86_64-linux-tinfo6/lts-12.18/8.4.4/bin/imageCompressor

all:
				stack build
				cp $(EXEC_PATH) .

clean:
				rm -f imageCompressor.cabal
				rm -f imageCompressor

fclean:			clean
				rm -rf .stack-work

re:             fclean all

.PHONY:         all fclean clean re
