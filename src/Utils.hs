module Utils where

import Parser
import System.Exit

euclidianDistance :: Point -> Cluster -> Double
euclidianDistance point cluster = sqrt(x'*x' + y'*y' + z'*z')
    where
        x' = (r point) - (red cluster)
        y' = (g point) - (green cluster)
        z' = (b point) - (blue cluster)

euclidianDistanceClusters :: Cluster -> Cluster -> Double
euclidianDistanceClusters cluster1 cluster2 = sqrt(x'*x' + y'*y' + z'*z')
    where
        x' = (red cluster1) - (red cluster2)
        y' = (green cluster1) - (green cluster2)
        z' = (blue cluster1) - (blue cluster2)

printPoints :: Cluster -> [Point] -> IO()
printPoints cluster points = do
    if length points > 0 then do
        let point = ((take 1 points) !! 0)
        if cluster == (c point) then do
            let color = (floor (r point), floor (g point), floor (b point))
            putStr ("(" ++ show (x point) ++ "," ++ show (y point) ++ ") ")
            print color
        else do
            putStr ""
        printPoints cluster newPoints
    else do
        putStr ""
    where
        newPoints = drop 1 points

printClusters :: [Cluster] -> [Point] -> IO()
printClusters clusters points = do
    if length clusters > 0 then do
        let cluster = ((take 1 clusters) !! 0)
        let line = ((red cluster), (green cluster), (blue cluster))
        putStrLn "--"
        print line
        putStrLn "-"
        printPoints cluster points
        printClusters newClusters points
    else do
        putStr ""
    where
        newClusters = drop 1 clusters

printUsage = do
    putStrLn "USAGE: ./imageCompressor n e IN"
    putStrLn ""
    putStrLn "     n     number of colors in the final image"
    putStrLn "     e     convergence limit"
    putStrLn "     IN    path to the file containing the colors of the pixels"
