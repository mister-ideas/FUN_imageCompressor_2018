module Clustering where

import System.Random
import Parser
import Utils

initClusters :: [Point] -> [Cluster] -> Int -> [Cluster]
initClusters points centroids number = do
    if length centroids /= number then do
        let point = (points !! (number - length centroids - 1))
        let cluster = Cluster (r point) (g point) (b point)
        initClusters points (centroids ++ [cluster]) number
    else do
        centroids

assignClusterToPoint :: Point -> [Point] -> [Cluster] -> Double -> [Point]
assignClusterToPoint point updatedPoint clusters prevDist = do
    if length clusters > 0 then do
        let cluster = ((take 1 clusters) !! 0)
        let dist = euclidianDistance point cluster
        if dist > prevDist && prevDist /= (-1) then do
            assignClusterToPoint point updatedPoint newClusters prevDist
        else do
            assignClusterToPoint point ([(Point (x point) (y point) (r point) (g point) (b point) cluster)]) newClusters dist
    else do
        updatedPoint
    where
        newClusters = drop 1 clusters

updatePoints :: [Point] -> [Point] -> [Cluster] -> [Point]
updatePoints points updatedPoints centroids = do
    if length points > 0 then do
        let point = ((take 1 points) !! 0)
        updatePoints newPoints (updatedPoints ++ (assignClusterToPoint point [] centroids (-1))) centroids
    else do
        updatedPoints
    where
        newPoints = drop 1 points

updateClustersPosition :: Cluster -> [Point] -> Color -> Double -> Cluster
updateClustersPosition cluster points updatedPos number = do
    if length points > 0 then do
        let point = ((take 1 points) !! 0)
        if cluster == (c point) then do
            let newPos = ((firstColor updatedPos) + (r point), (secondColor updatedPos) + (g point), (thirdColor updatedPos) + (b point))
            let newNumber = number + 1
            updateClustersPosition cluster newPoints newPos newNumber
        else do
            updateClustersPosition cluster newPoints updatedPos number
    else do
        let updateCluster = Cluster ((firstColor updatedPos) / number) ((secondColor updatedPos) / number) ((thirdColor updatedPos) / number)
        updateCluster
    where
        newPoints = drop 1 points

updateClusters :: [Cluster] -> [Cluster] -> [Point] -> [Cluster]
updateClusters clusters updatedClusters points = do
    if length clusters > 0 then do
        let cluster = ((take 1 clusters) !! 0)
        updateClusters newClusters (updatedClusters ++ [(updateClustersPosition cluster points (0, 0, 0) 0)]) points
    else do
        updatedClusters
    where
        newClusters = drop 1 clusters

checkConvergenceLimit :: [Cluster] -> [Cluster] -> Double -> Double -> Bool
checkConvergenceLimit clusters prevClusters prevDist convergenceLimit = do
    if length clusters > 0 && length prevClusters > 0 then do
        let cluster = ((take 1 clusters) !! 0)
        let prevCluster = ((take 1 prevClusters) !! 0)
        let dist = euclidianDistanceClusters cluster prevCluster
        if dist < prevDist && prevDist /= (-1) then do
            checkConvergenceLimit newClusters newPrevClusters prevDist convergenceLimit
        else do
            checkConvergenceLimit newClusters newPrevClusters dist convergenceLimit
    else do
        if prevDist > convergenceLimit then do
            False
        else do
            True
    where
        newClusters = drop 1 clusters
        newPrevClusters = drop 1 prevClusters

kmeans :: [Cluster] -> [Cluster] -> [Point] -> Int -> Double -> IO()
kmeans clusters prevClusters points clustersNumber convergenceLimit = do
    if checkConvergenceLimit clusters prevClusters (-1) convergenceLimit == True && prevClusters /= [] then do
        printClusters clusters points
    else do
        let updatedClusters = updateClusters clusters [] points
        let updatedPoints = updatePoints points [] updatedClusters
        kmeans updatedClusters clusters updatedPoints clustersNumber convergenceLimit