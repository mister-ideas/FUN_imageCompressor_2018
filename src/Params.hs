module Params where

import System.Environment
import System.Exit
import Utils
import Parser
import Clustering

processParams = do
    args <- getArgs
    let nb = length args
    if nb /= 3 then do printUsage ; exitWith (ExitFailure 84)
    else do
        file <- readFile (args !! 2)
        let points = parseFile (words file) []
        let clustersNumber = read (args !! 0) :: Int
        let convergenceLimit = read (args !! 1) :: Double
        if length points < clustersNumber || clustersNumber < 0 || convergenceLimit < 0 then do
            printUsage ; exitWith (ExitFailure 84)
        else do
            let clusters = initClusters points [] clustersNumber
            let updatedPoints = updatePoints points [] clusters
            kmeans clusters [] updatedPoints clustersNumber convergenceLimit