module Parser where

type Coords = (Int, Int)
type Color = (Double, Double, Double)
data Point = Point
    { x :: Int
    , y :: Int
    , r :: Double
    , g :: Double
    , b :: Double
    , c :: Cluster
    } deriving (Read, Show)
data Cluster = Cluster
    { red :: Double
    , green :: Double
    , blue :: Double
    } deriving (Read, Show, Eq)

firstColor :: Color -> Double
firstColor (r,_,_) = r

secondColor :: Color -> Double
secondColor (_,g,_) = g

thirdColor :: Color -> Double
thirdColor (_,_,b) = b

parseFile :: [String] -> [Point] -> [Point]
parseFile tokens points = do
    if (length tokens) > 0 then do
        let coords = read (tokens !! 0) :: Coords
        let color = read (tokens !! 1) :: Color
        let cluster = Cluster (-1) (-1) (-1)
        let point = Point (fst coords) (snd coords) (firstColor color) (secondColor color) (thirdColor color) cluster :: Point
        parseFile newList (points ++ [point])
    else
        points
    where
        newList = drop 2 tokens